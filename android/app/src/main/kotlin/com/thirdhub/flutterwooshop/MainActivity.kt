package com.thirdhub.flutterwooshop

import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant
import android.util.Log

import java.security.NoSuchAlgorithmException

import java.security.MessageDigest

import android.content.pm.PackageManager

import android.content.pm.PackageInfo

import android.content.Context




class MainActivity: FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
    }
}
